/*
  Input Pull-up Serial
  ROCKBAND
*/

int buttonState1 = 0;         // variable for reading the pushbutton status
int previousButtonState1 = 0;
int buttonState2 = 0;         // variable for reading the pushbutton status
int previousButtonState2 = 0;
int buttonState3 = 0;         // variable for reading the pushbutton status
int previousButtonState3 = 0;
int buttonState4 = 0;         // variable for reading the pushbutton status
int previousButtonState4 = 0;

void setup() {
  //start serial connection
  Serial.begin(9600);
  //configure pin 2 as an input and enable the internal pull-up resistor
  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(4, INPUT_PULLUP);
  pinMode(5, INPUT_PULLUP);
  //
}

void loop() {
  //read the pushbutton value into a variable
  buttonState1 = digitalRead(2);
  buttonState2 = digitalRead(3);
  buttonState3 = digitalRead(4);
  buttonState4 = digitalRead(5);
    

  ///////////////
  //////////// btn1 bleu
  if (buttonState1 == HIGH) {
    Serial.print("btn1 ");
    Serial.println(buttonState1);
    } else {
    Serial.print("btn1 ");
    Serial.println(buttonState1);
  }
    //////////// btn2 jaune
  if (buttonState2 == HIGH) {
    Serial.print("btn2 ");
    Serial.println(buttonState2);
    } else {
    Serial.print("btn2 ");
    Serial.println(buttonState2);
  }
    //////////// btn3 orange
  if (buttonState3 == HIGH) {
    Serial.print("btn3 ");
    Serial.println(buttonState3);
    } else {
    Serial.print("btn3 ");
    Serial.println(buttonState3);
  }
    //////////// btn4 rouge
  if (buttonState4 == HIGH) {
    Serial.print("btn4 ");
    Serial.println(buttonState4);
    } else {
    Serial.print("btn4 ");
    Serial.println(buttonState4);
  }


}
